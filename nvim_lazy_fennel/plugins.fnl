(local lib (require :core.lib))

(lib.flatten_lists [
    (require :plugins.syntax)
    (require :plugins.colors)
    (require :plugins.ui)
    (require :plugins.editing)
    (require :plugins.navigation)
    (require :plugins.leading_commands)

    (require :plugins.git)
    (require :plugins.terminal)
    (require :plugins.filemanager)
    (require :plugins.telescope)
    (require :plugins.development)

    (require :plugins.cpp)
    (require :plugins.notes)
    ])
