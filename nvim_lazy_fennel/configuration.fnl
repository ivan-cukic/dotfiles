(local x (require :core.xvim))

(x.disable_plugins [
    :gzip
    :zip
    :zipPlugin
    :tar
    :tarPlugin

    :getscript
    :getscriptPlugin
    :vimball
    :vimballPlugin
    :2html_plugin

    :netrw
    :netrwPlugin
    :netrwSettings
    :netrwFileHandlers
    ])

(x.set_globals {
    :username "Ivan Čukić"
    :email    "ivan AT cukic.co"

    :mapleader " "
    })

(x.set_options {
    ;; Visual options
    :termguicolors true
    :background "dark"
    :colorcolumn :80

    ;; For keeping me sane
    :autowrite true ;; save before calling make
    :wrap false ;; don't wrap the text
    :number true ;; show line numbers
    :cursorline true ;; highlight current line
    :scrolloff 2 ;; scroll offset
    :wildmenu true ;; tab completion for commands
    :completeopt "menu" ;; show completions in a menu

    ;; Searching
    :ignorecase true ;; ignore case when searching ...
    :smartcase true ;; ... but be smart about it

    ;; Indentation
    :autoindent true
    :cindent true
    :expandtab true
    :tabstop 4
    :softtabstop 4
    :shiftwidth 4
    :list true
    :listchars { :trail "·"
                 :tab "»·"
                 :extends "»" }

    ;; Line wrapping
    :breakindent true
    :showbreak "╳╳╳╳" ;▧▧▧▧"

    ;; always have statusline
    :statusline :2

    ;; makeobj is our default make
    :makeprg :makeobj

    ;; mouse? no.
    :mouse ""
    })

(x.vim_exec [
    "setlocal cinkeys-=:"
])
