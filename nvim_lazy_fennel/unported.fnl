(d.load_plugins [
    ;; File browsing / file manager
    {   :use :lambdalisue/fern.vim
        :globals { :fern#renderer :nerdfont }
    }
    :lambdalisue/glyph-palette.vim
    :lambdalisue/nerdfont.vim
    :lambdalisue/fern-renderer-nerdfont.vim
    :lambdalisue/fern-hijack.vim
    :lambdalisue/fern-git-status.vim

    {   :use :not_a_plugin ; Fern mapping
        :post (lambda [] (d.vim_exec [
            (.. "source " (. vim.g :nvim_profile_path) "vim/fern-init.vim")
        ]))
    }

    ;; ViFM file manager integration (TODO: instead of Fern?)
    :vifm/vifm.vim

    ;; Runs clazy on the current file
    {   :use :not_a_plugin
        :post (lambda []
            (global execute_clazy_standalone_on_current_file (fn []
                (vim.cmd ":cgetexpr system('clazy-standalone --checks=level2 ' . shellescape(expand('%:p')))")
                (vim.cmd ":copen")
            ))
        )
    }

])
