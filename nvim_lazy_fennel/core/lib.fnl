
(fn map_list [function items]
    (each [_ item (ipairs items)]
        (function item)))

(fn merge_lists [a b]
    (local ab [])
    (table.move a 1 (length a) 1 ab)
    (table.move b 1 (length b) (+ (length ab) 1) ab)
    ab)

(fn flatten_lists [lists]
    (var result [])
    (each [_ item (ipairs lists)]
        (set result (merge_lists result item)))
    result)

{: map_list
 : merge_lists
 : flatten_lists }
