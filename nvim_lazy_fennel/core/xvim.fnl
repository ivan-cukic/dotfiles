(import-macros xlang :core.macros.xlang)

(local lib (require :core.lib))

(fn use_core_plugin [ name url path init ]
    (xlang.unless (vim.loop.fs_stat path)
        (vim.notify (.. name ": We need to clone " url))
        (vim.fn.system [ "git" "clone" "--filter=blob:none" "--single-branch" url path ])
    )
    (vim.opt.runtimepath:append path)
    (init (require name))
)

(fn vim_exec [cmds]
    (lib.map_list vim.api.nvim_command cmds))

(fn set_global [name value]
    "vim.g = ???"
    (tset vim.g name value))

(fn set_globals [kvs]
    (each [field value (pairs kvs)]
        (set_global field value)))

(fn set_option [name value]
    "vim.opt = ???"
    (tset vim.opt name value))

(fn set_options [kvs]
    (each [field value (pairs kvs)]
        (set_option field value)))

(fn init_paths [profile_name]
    (local user_home (os.getenv :HOME))
    (local nvim_name (.. "nvim_" profile_name))
    (local nvim_profile_path (.. user_home "/." nvim_name))
    (local nvim_data_path (.. user_home "/.local/share/" nvim_name))

    (set_globals {
        :user_home user_home

        :nvim_profile_path nvim_profile_path

        :lazy_path       (.. nvim_data_path "/core_plugins/lazy")
        :plugins_path    (.. nvim_data_path "/plugins")
        :compiled_fennel (.. nvim_data_path "/compiled_fennel")
    })
)

(fn disable_plugin [plugin]
    (set_global (.. "loaded_" plugin) 1))

(fn disable_plugins [plugins]
    (lib.map_list disable_plugin plugins))

(fn map_key [mode what to]
    (vim.api.nvim_set_keymap mode what to { :noremap false }))

(fn noremap_key [mode what to]
    (vim.api.nvim_set_keymap mode what to { :noremap true }))

(fn map_leader [mappings]
    (each [what to (pairs mappings)]
        (noremap_key :n (.. "<Leader>" what) to)))

(fn add_highlights [mappings]
    (each [what to (pairs mappings)]
        (vim_exec [ (.. "highlight " what " " to) ])))

(fn new_command [what to]
    (vim.cmd (.. "command " what " " to)))

(fn new_commands [cmds]
    (each [what to (pairs cmds)]
        (new_command what to)))

;; Just syntax sugar for defining the plugin name for lazy
(local use 1)

(fn preprocess_plugin_better [plugin]
    (fn add_step [old step]
        (if (= nil old)
            step
            (fn []
                (old)
                (step))))

    (var init nil)

    (match plugin {:globals plugin_globals} (set init (add_step init (lambda [] (set_globals plugin_globals))))
                  _                  nil)
    (match plugin {:highlights plugin_highlights} (set init (add_step init (lambda [] (add_highlights plugin_highlights))))
                  _                  nil)
    (match plugin {:init plugin_init} (set init (add_step init (lambda [] (plugin_init))))
                  _                  nil)

    (if (= nil init) nil (tset plugin :init init))
    plugin
)

(fn preprocess_plugin [plugin]
    (match plugin {:globals plugin_globals} (set_globals plugin_globals)
                  _                  nil)
    (match plugin {:highlights plugin_highlights} (add_highlights plugin_highlights)
                  _                  nil)

    plugin
)

(fn preprocess_plugins [plugins]
    (lib.map_list preprocess_plugin_better plugins)
)

{: use_core_plugin
 : set_global
 : set_globals
 : set_option
 : set_options
 : init_paths
 : disable_plugin
 : disable_plugins
 : map_leader
 : vim_exec
 : new_command
 : new_commands
 : add_highlights
 : use
 : preprocess_plugin
 : preprocess_plugins
}
