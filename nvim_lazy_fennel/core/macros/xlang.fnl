{
    :unless (fn [condition body ...]
        `(if (not ,condition)
            (do
                ,body
                ,...)))


}
