(local x (require :core.xvim))

(x.new_commands {
    :Fix             "Lspsaga code_action"
    :LineDiagnostics "Lspsaga show_line_diagnostics"
    :Doc             "lua vim.lsp.buf.hover()"
    :References      "Lspsaga lsp_finder"
    :Grep            ":lua require(\"telescope\").extensions.live_grep_args.live_grep_args()"
    :LazyUpdate      ":lua require(\"lazy\").update()"

    :CppMan          ":lua require'telescope_zeal'.show('cpp')"
    :QtMan           ":lua require'telescope_zeal'.show('qt5')"
    :QtMan6          ":lua require'telescope_zeal'.show('qt6')"
    :BoostMan        ":lua require'telescope_zeal'.show('boost')"
    :CMakeMan        ":lua require'telescope_zeal'.show('cmake')"

    :Commands        ":lua vim.g.show_commands_menu()"
    :ToggleActions   ":lua vim.g.show_toggle_options()"

    :Manpages        "Telescope man_pages"
    :C               "copen | resize 40"
    :FernOpenSidebar "Fern -drawer -reveal=% ."

    ;; Forgetting to release shift can be annoying
    :W  ":w"
    :Wa ":wa"
    :X  ":x"
    :Xa ":xa"
    :Q  ":q"
    :Qa ":qa"
    })
