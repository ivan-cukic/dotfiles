(local x (require :core.xvim))

(x.map_leader {
    ;; Saving files and quitting
    :q ":q<CR>"
    :w ":w<CR>"
    :x ":x<CR>"

    ;; Tab navigation
    :n ":tabnext<CR>"
    :p ":tabprev<CR>"
    :N ":tabnew<CR>"

    ;; Splits navigation
    :<Left>  "<C-w><Left>"
    :<Right> "<C-w><Right>"
    :<Up>    "<C-w><Up>"
    :<Down>  "<C-w><Down>"

    :v ":split<CR>"
    :h ":vsplit<CR>"

    :a ":lua telescope_switch()<CR>" ;; Switch between .h .cpp
    :g ":Ge:<CR>" ;; Fugitive sumary
    :o ":Telescope find_files find_command=rg,--ignore-file=~/.rg-global-ignore,--hidden,--files <CR>"
    :M ":Telescope git_status<CR>" ;; Open a modified file
    :s ":Telescope live_grep<CR>" ;; Open a modified file
    :m ":lua telescope_show_modified_on_branch()<CR>" ;; Open a file modified on current branch
    :r ":References<CR>" ;; References
    :f ":Fix<CR>" ;; Code actions
    :c ":Make<CR>" ;; Compile the current project
    :t ":Commands<CR>"
    :u ":ToggleActions<CR>"

    :b ":BookmarkToggle<CR>"
    :B ":BookmarkAnnotate<CR>"
    :/ ":Telescope vim_bookmarks current_file<CR>"
    :? ":Telescope vim_bookmarks<CR>"
    :z ":Zk<CR>"

    :<Leader> "<Esc>:HopLineStart<CR>"
    :w        "<Esc>:HopWord<CR>"
    })

(x.vim_exec [
    "nnoremap <silent> <leader>8 :call InterestingWords('n')<cr>"
    "vnoremap <silent> <leader>8 :call InterestingWords('v')<cr>"
    "nnoremap <silent> <leader>9 :call UncolorAllWords()<cr>"
    "nnoremap <silent> n :call WordNavigation(1)<cr>"
    "nnoremap <silent> N :call WordNavigation(0)<cr>"

    ;; Terminal maps
    "tnoremap <Esc> <C-\\><C-n>"
    "tnoremap <C-d> <C-\\><C-n>"
    "tmap <C-l> <C-l><Esc>:set scrollback=1<CR>:set scrollback=0<CR>i"

    "tnoremap <A-Left> <C-\\><C-N><C-w>h"
    "tnoremap <A-Down> <C-\\><C-N><C-w>j"
    "tnoremap <A-Up> <C-\\><C-N><C-w>k"
    "tnoremap <A-Right> <C-\\><C-N><C-w>l"
    "tnoremap <S-A-Up> <C-\\><C-N><C-w>:tabnew<CR>"
    "tnoremap <S-A-Right> <C-\\><C-N><C-w>:tabnext<CR>"
    "tnoremap <S-A-Left> <C-\\><C-N><C-w>:tabprev<CR>"

    "inoremap <A-Left> <C-\\><C-N><C-w>h"
    "inoremap <A-Down> <C-\\><C-N><C-w>j"
    "inoremap <A-Up> <C-\\><C-N><C-w>k"
    "inoremap <A-Right> <C-\\><C-N><C-w>l"
    "inoremap <S-A-Up> <C-\\><C-N><C-w>:tabnew<CR>"
    "inoremap <S-A-Right> <C-\\><C-N><C-w>:tabnext<CR>"
    "inoremap <S-A-Left> <C-\\><C-N><C-w>:tabprev<CR>"

    "nnoremap <A-Left> <C-w>h"
    "nnoremap <A-Down> <C-w>j"
    "nnoremap <A-Up> <C-w>k"
    "nnoremap <A-Right> <C-w>l"
    "nnoremap <S-A-Up> <Esc>:tabnew<CR>"
    "nnoremap <S-A-Right> <Esc>:tabnext<CR>"
    "nnoremap <S-A-Left> <Esc>:tabprev<CR>"

    "vnoremap zn <Esc>:'<,'>ZkNewFromTitleSelection<CR>"
    "vnoremap <leader>z :lua vim.g.zk_show_menu_visual()<CR>"
    ])
