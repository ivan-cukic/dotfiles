(require :configuration)
(require :keymaps)
(require :commands)

(local x (require :core.xvim))

(x.set_globals { :mapleader " " })

;; This is the name of our profile
(x.init_paths "lazy_fennel")

;; Load plugins definition
(local plugins (require :plugins))

;; We have some custom plugin definition fields that
;; need to be translated to what lazy.nvim understands
(x.preprocess_plugins plugins)

(x.use_core_plugin
    :lazy "https://github.com/folke/lazy.nvim.git" vim.g.lazy_path
    (fn [lazy] (lazy.setup plugins { :root vim.g.plugins_path })))
