(local x (require :core.xvim))

[
    ;; Show indentation lines
    ; {   x.use :Yggdroot/indentLine
    ;     :global { :indentLine_char "⸽"
    ;               :indentLine_fileTypeExclude [ :vimwiki :markdown ]
    ;               :indentLine_concealcursor "" }
    ; }

    ;; Testing as a replacement for indentLine,
    ;; so far, looks great.
    {   x.use :shellRaining/hlchunk.nvim
        :event [ :UIEnter ]
        :opts {
            :indent {
                :chars [ "│" ]
            }
            :style [ "#8B00FF" ]
            :blank { :enable false }
        }
    }

    {   x.use :andersevenrud/nvim_context_vt
        :opts {
            :prefix "↖"
            :highlight :Comment
        }
    }

    ;; Fancy icons for NVIM
    :kyazdani42/nvim-web-devicons

    ;; Highlights the word under the cursor
    :RRethy/vim-illuminate

    ;; Multiple separate search highlights
    {   x.use :lfv89/vim-interestingwords
        :globals { :interestingWordsRandomiseColors 1
                   :interestingWordsDefaultMappings 0 }
    }

    ;; A better quick fix window
    ;; Shows floats with the referenced code
    {   x.use :kevinhwang91/nvim-bqf
        :module :bqf
        :opts {
            :auto_enable true
            :auto_resize_height true
            :preview { :win_height 30 }
        }
    }

    {   x.use :nvim-lualine/lualine.nvim
        :opts {
            :options {
                ; :theme :base16
                ; :theme :material
                ; :theme :everfrost ; nice
                ; :theme :powerline ; highlighted in insert mode
                ; :theme :wombat ; pretty, low visibility file name
                ; :theme :seoul256 ; good visibility
                :theme :onedark ; subtle, ok visibility
                ; :section_separators { :left "" :right "" }
                :component_separators { :left "" :right "" }
            }
            :sections {
                :lualine_a [ :mode ]
                :lualine_b [ :filename ]
                :lualine_c [ ]
                :lualine_x [ { 1 :FugitiveHead :icon "" } :diff ]
                :lualine_y [ :progress :location ]
                :lualine_z [ ]
            }
            :winbar {
                :lualine_a [ :hostname ]
                :lualine_b [ {
                    1 :aerial
                    :sep "  "
                } ]
                :lualine_c [ "\" \"" ]
                :lualine_y [ :diagnostics ]
                :lualine_z [ "\"  \"" ]
            }
            :inactive_winbar {
                :lualine_a [ "\"   \"" ]
            }
            :extensions [ :aerial ]
        }
    }

    ;; Different backgrounds for active and inactive windows
    {   :url "https://gitlab.com/ivan-cukic/vim-dim-inactive"
    }
]
