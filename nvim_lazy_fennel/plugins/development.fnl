(local x (require :core.xvim))

[
    {   x.use :luckasRanarison/nvim-devdocs
        :opts {}
    }

    {   x.use :tpope/vim-dispatch
    }
]
