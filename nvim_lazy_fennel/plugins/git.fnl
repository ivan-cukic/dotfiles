(local x (require :core.xvim))

[
    ;; The best Git client
    :tpope/vim-fugitive

    ;; Show changed lines from the last commit
    {   x.use :lewis6991/gitsigns.nvim
        :dependencies :nvim-lua/plenary.nvim
        :opts {
            :numhl true
            :signs {
                :add { :hl     "GitSignsAdd"
                       :text   "▐"
                       :numhl  "GitSignsAddNr"
                       :linehl "GitSignsAddLn" }
                :change { :hl     "GitSignsChange"
                          :text   "▚" ; ▐"
                          :numhl  "GitSignsChangeNr"
                          :linehl "GitSignsChangeLn" }
                :delete { :hl     "GitSignsDelete"
                          :text   "▂"
                          :numhl  "GitSignsDeleteNr"
                          :linehl "GitSignsDeleteLn" }
                :topdelete { :hl     "GitSignsDelete"
                             :text   "▔"
                             :numhl  "GitSignsDeleteNr"
                             :linehl "GitSignsDeleteLn" }
                :changedelete { :hl     "GitSignsChange"
                                :text   "~"
                                :numhl  "GitSignsChangeNr"
                                :linehl "GitSignsChangeLn" }
            }
        }
    }
]
