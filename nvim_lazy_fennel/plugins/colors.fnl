(local x (require :core.xvim))

[
    ; :ayu-theme/ayu-vim
    ; :josegamez82/starrynight
    ; :ozkanonur/nimda.vim

    :mhartington/oceanic-next
    :AlexvZyl/nordic.nvim
    :nanotech/jellybeans.vim

    {   x.use :EdenEast/nightfox.nvim ;; terafox is not bad
        :opts { :inverse { :match_paren true
                           :search      true }
                :palettes { :terafox { :sel0 "#505050"
                                       :sel1 "#d8a657" } }
        }

        :config (lambda [] (x.vim_exec [
            ;; "let ayucolor=\"dark\""
            ;; "colorscheme desert"
            ;; "colorscheme ayu"
            ;; "colorscheme OceanicNext"
            ;; "colorscheme nordic"
            ;; "colorscheme terafox"

            ;; "highlight ColorColumn term=reverse ctermbg=88 ctermfg=red"

            ;; "highlight BooleanFalse guifg=#ff4433 gui=bold"
            ;; "highlight BooleanTrue guifg=lightgreen gui=bold"

            ;; "highlight LineNr guifg=#505050"
        ]))
    }

    {   x.use :sainnhe/sonokai
        :globals { :sonokai_style :maia }
        :config (lambda [] (x.vim_exec [
            "colorscheme sonokai"

            "highlight ColorColumn term=reverse ctermbg=88 ctermfg=red"

            "highlight BooleanFalse guifg=#ff4433 gui=bold"
            "highlight BooleanTrue guifg=lightgreen gui=bold"

            "highlight LineNr guifg=#505050"
        ]))
    }
]
