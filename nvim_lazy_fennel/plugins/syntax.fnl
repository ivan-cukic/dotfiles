(local x (require :core.xvim))

[
    ;; Fennel syntax
    :bakpakin/fennel.vim

    ;; QML syntax
    :peterhoeg/vim-qml

    ;; CMake syntax
    :jansenm/vim-cmake

    ;; TreeSitter plugins
    {   x.use :nvim-treesitter/nvim-treesitter
        :opts {
            :run ":TSUpdate"
        }
        :dependencies [
            :nvim-treesitter/playground
            :p00f/nvim-ts-rainbow
        ]

        :config (lambda []
            (local tsc (require :nvim-treesitter.configs))
            (tsc.setup {
                :ensure_installed [ :cpp :python ]
                :highlight { :enable false
                             :additional_vim_regex_highlighting false }
                :rainbow { :enable false
                           :extended_mode true }
                :context { :enable false
                           :throttle true }
                :playground { :enable false
                              :disable {}
                              :updatetime 25
                              :persist_queries false }
            })
        )
    }
]
