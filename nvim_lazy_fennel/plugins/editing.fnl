(local x (require :core.xvim))

[
    ;; Trim whitespace from EOL
    {   x.use :cappyzawa/trim.nvim
        :opts {}
    }

    ;; Comment -- commenting selections out
    {   x.use :numToStr/Comment.nvim
        :opts { :toggler { :line "__" :block "++" }
                :opleader { :line "_" :block "+" } }
    }

    ;; Snippets galore
    {   x.use :dcampos/nvim-snippy
        :opts {} ;; set in cmp
    }

    ;; Formatting (clang-format and friends)
    {   x.use :sbdchd/neoformat
        :config (lambda []
            (x.vim_exec [
                "augroup fmt"
                "autocmd!"
                "autocmd BufWritePre * undojoin | Neoformat"
                "augroup END"
            ])
        )
    }
]
