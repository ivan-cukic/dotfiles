(local x (require :core.xvim))

(global open_side_term (fn []
    (x.vim_exec [
        "belowright 40 vsplit"
        "set wfw"
        "Neomux"
    ])))

(x.new_command :OpenSideTerminal ":lua open_side_term()")

[
    {   x.use :nikvdp/neomux
        :globals { :neomux_start_term_map "<Leader>>"
                   :neomux_winjump_map_prefix "<Leader>"
                   :neomux_default_shell "zsh"
                   :neomux_win_num_status "∥ W:%{WindowNumber()} ∥"
                   :neomux_dont_fix_term_ctrlw_map 1 }
    }
]
