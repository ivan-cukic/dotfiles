(local x (require :core.xvim))

[
    {   x.use :mickael-menu/zk-nvim
        :main :zk
        :config (fn []
            (local zk (require :zk))
            (local zk_cmds (require :zk.commands))
            (local x (require :core.xvim))

            (zk.setup { :picker :telescope })

            ;; A few useful commands
            (fn make_edit_fn [defaults picker_options]
                (fn [options]
                    (var new_options (vim.tbl_extend :force defaults (or options {})))
                    (zk.edit new_options picker_options)
            ))

            (zk_cmds.add :ZkOrphans (make_edit_fn { :orphan true } { :title "Zk Orphans" }))
            (zk_cmds.add :ZkRecents (make_edit_fn { :createdAfter "2 weeks ago" } { :title "Zk Recents" }))
            (zk_cmds.add :ZkToday (make_edit_fn { :createdAfter "1 days ago" } { :title "Zk Recents" }))

            ;; Make the new note command which asks for the note title
            ;; as the title will end up in the file name
            (x.set_global :zk_new_note (fn []
                (vim.ui.input { :prompt "Enter note title: " } (fn [input]
                    (zk.new { :title input }))
            )))
            (x.new_commands { :ZkNewNote ":lua vim.g.zk_new_note()" })

            ;; Menu for Zk-related commands
            (x.set_global :zk_show_menu (fn []
                (local commands [
                    "Zk"
                    [ :n "ZkNewNote" "New note" ]
                    ""
                    [ :d "ZkToday" "Today" ]
                    [ :r "ZkRecents" "Recents" ]
                    [ :z "ZkNotes" "All" ]
                    ""
                    [ :t "ZkTags" "Tags" ]
                    ""
                    [ :b "ZkBacklinks" "Backlinks" ]
                    [ :l "ZkLinks" "Links" ]
                    ])
                (local leading_commands (require :leading_commands))
                (leading_commands.show_commands_menu commands)))

            (x.new_commands { :Zk ":lua vim.g.zk_show_menu()" })

            ;; Menu for Zk-related commands
            (x.set_global :zk_show_menu_visual (fn []
                (local commands [
                    "Zk"
                    [ :n "'<,'>ZkNewFromTitleSelection" "New note (selection as title)" ]
                    ])
                (local leading_commands (require :leading_commands))
                (leading_commands.show_commands_menu commands)))
        )
    }
]
