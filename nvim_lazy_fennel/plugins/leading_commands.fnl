(local x (require :core.xvim))

[
    {   :url "https://gitlab.com/ivan-cukic/nvim-leading-commands"
        :config (lambda []
            (x.set_global :show_commands_menu (fn []
                (local commands [
                    "Symbols"
                    [ :x ":References" "Find usages" ]
                    [ :r ":Rename" "Rename" ]
                    [ :t ":Telescope aerial" "In the current file" ]
                    [ :T ":Telescope lsp_dynamic_workspace_symbols" "In the project" ]

                    ""
                    "Code issues"
                    [ :z ":Clazy" "Run Clazy on current file" ]
                    [ :l ":LineDiagnostics" "Current line diagnostics" ]
                    [ :e ":Trouble document_diagnostics" "Current file" ]
                    [ :E ":Trouble workspace_diagnostics" "Project" ]
                    [ :q ":Trouble quickfix" "Last compilation" ]

                    ""
                    "Documentation"
                    [ :h ":Doc" "Documentation" ]
                    [ :c ":CppMan" "C++ Reference" ]
                    [ :m ":CMakeMan" "CMake Reference" ]
                    [ :5 ":QtMan" "Qt5 manual pages" ]
                    [ :6 ":QtMan6" "Qt6 manual pages" ]

                    [ :/ ":Telescope current_buffer_fuzzy_find" "Fuzzy search current file" ]
                    [ :X ":Telescope search_history" "Search history" ]
                    [ :X ":Telescope command_history" "Command history" ]

                    ;; "Ignore"
                    ;; [ :f ":Fix" "Fix-it (use `<leader>f` instead)" ]
                    ;; :X ":Lspsaga signature_help" ;; doesn't work for C++ it seems
                    ;; :X ":Lspsaga preview_definition" ;; not needed, we have :References

                    ])

                (local leading_commands (require :leading_commands))
                (leading_commands.show_commands_menu commands)))

            (x.set_global :show_toggle_options (fn []
                (local commands [
                    "Toggle"
                    [ :u "AerialOpen" "Jump to Aerial (TOC)" ]
                    [ :t "AerialToggle" "Aerial (TOC)" ]
                    [ :g "Gitsigns toggle_current_line_blame" "Git Blame line" ]
                    ])
                (local leading_commands (require :leading_commands))
                (leading_commands.show_commands_menu commands)))
        )
    }
]
