(local x (require :core.xvim))

[
    {   x.use :phaazon/hop.nvim
        :args  { :branch :v1 }
        :opts  { :keys "asdfghjklqwertyuiopzxcvbnm" }
        :highlights { :HopNextKey  "guifg=#f0f00f"
                      :HopNextKey1 "guifg=#f0f00f"
                      :HopNextKey2 "guifg=#f0f00f" }
    }

    {   x.use :MattesGroeger/vim-bookmarks
        :globals { :bookmark_sign "♥ "
                   :bookmark_annotation_sign "☰ "
                   :bookmark_highlight_lines 1
                   :bookmark_no_default_key_mappings 1
                   :bookmark_save_per_working_dir 1
                   :bookmark_auto_save 1 }
        :highlights { :BookmarkSign "guibg=#01afe1 guifg=white"
                      :BookmarkLine "guibg=#0c2c3c"
                      :BookmarkAnnotationSign "guibg=#01afe1 guifg=white"
                      :BookmarkAnnotationLine "guibg=#0c2c33" }
        :config (lambda [] (x.vim_exec [ ;; Bookmarks custom per-project path
            (.. "source " (. vim.g :nvim_dotfiles_path) "/vim/vim-bookmarks-custom-path.vim")
        ]))
    }

    {   x.use :stevearc/aerial.nvim
        :opts {
            :backends [ :lsp :treesitter ]
            :icons {
                :Namespace "󰙅"
                :Class ""
                :Function "󰅲"
                :Method "󰅲"
            }
            :filter_kind false
            ; :filter_kind [ :Namespace :Class :Constructor :Enum :Function :Interface :Module :Method :Struct ]
            :layout {
                :default_direction :left
                :max_width 25
                :min_width 25
                :placement :edge
            }
            :attach_mode :global
        }
    }
]
