(local x (require :core.xvim))

[
    {   x.use :bfrg/vim-cpp-modern
        :globals {
            :cpp_attributes_highlight 1
            :cpp_member_highlight 1
        }
    }

    :neovim/nvim-lspconfig
    :hrsh7th/cmp-nvim-lsp
    :hrsh7th/cmp-buffer
    :hrsh7th/cmp-path
    :hrsh7th/cmp-cmdline
    :dcampos/cmp-snippy

    {   x.use :hrsh7th/nvim-cmp
        :config (lambda []
            (let [ lsp (require :lspconfig)
                   cmp (require :cmp)
                   snippy (require :snippy)
                   cmp_nvim_lsp (require :cmp_nvim_lsp)
                   lsp_capabilities (vim.lsp.protocol.make_client_capabilities)
                   cmp_capabilities (cmp_nvim_lsp.default_capabilities)
                   has_words_before (lambda []
                       (global unpack (or unpack table.unpack))
                       (local (line col) (unpack (vim.api.nvim_win_get_cursor 0)))
                       (and (not= col 0) (= (: (: (. (vim.api.nvim_buf_get_lines 0 (- line 1) line true) 1) :sub col col) :match "%s") nil))
                   )
                ]
                (cmp.setup {
                    :mapping (cmp.mapping.preset.insert {
                        "<C-Space>" (cmp.mapping.complete)
                        "<C-e>"     (cmp.mapping.confirm { :select true })
                        ; "<TAB>"     (cmp.mapping.confirm { :select true })
                        "<TAB>"     (cmp.mapping (lambda [fallback]
                                        (if  (snippy.can_expand_or_advance) (snippy.expand_or_advance)
                                             (cmp.visible) (cmp.select_next_item)
                                             (has_words_before) (cmp.complete)
                                             (fallback))
                                        ) [:i :s])
                        "<CR>"      (cmp.mapping (lambda [fallback]
                                        (if  ;; (snippy.can_expand_or_advance) (snippy.expand_or_advance)
                                             (cmp.visible) (cmp.mapping.confirm { :select true })
                                             (fallback))
                                        ) [:i :s])
                    })
                    :snippet {
                        :expand (lambda [args] (snippy.expand_snippet args.body))
                    }
                    :sources (cmp.config.sources [
                        { :name :snippy }
                        { :name :nvim_lsp }
                        { :name :buffer }
                    ])
                })
                (lsp.clangd.setup {
                    ; all options are in clangd_extensions
                    ; including on_attach
                    :capabilities cmp_capabilities
                    :cmd [ "clangd-18" ]
                })
            )
        )
    }

    ;; Useful extra LSP features
    {   x.use :tami5/lspsaga.nvim
        :config (lambda []
            (local lspsaga (require :lspsaga))
            (lspsaga.init_lsp_saga {
                :error_sign ""
                :warn_sign  ""
                :hint_sign  "?"
                :infor_sign "i"

                :max_preview_lines 32
                :border_style      "round"

                :finder_action_keys {
                    :open   "o"
                    :vsplit "s"
                    :split  "i"
                    :quit   "<esc>"
                    :scroll_down "<C-f>"
                    :scroll_up   "<C-b>"
                }
                :code_action_keys {
                    :quit "<esc>"
                    :exec "<CR>"
                }
                :rename_action_keys {
                    :quit "<esc>"
                    :exec "<CR>"
                }
                :code_action_icon " "
            }))
    }

    ;; LSP errors in top right corner
    {   x.use :dgagn/diagflow.nvim
        :event :LspAttach
        :opts {}
    }

    ;; For generating statusline components
    {   x.use :nvim-lua/lsp-status.nvim
        :config (lambda []
            (local lsp_status (require :lsp-status))
            (lsp_status.register_progress)
            (lsp_status.config {
                :indicator_errors   "E"
                :indicator_warnings "W"
                :indicator_info     "i"
                :indicator_hint     "?"
                :indicator_ok       "Ok"
                :current_function   true
            })
        )
    }

    ;; Automatically run clang-format on file save
    ; See editing.fnl / neoformat
    ; :rhysd/vim-clang-format

    {   x.use :p00f/clangd_extensions.nvim
        :opts {
            :server {
                :cmd [ "/usr/bin/clangd-17" "--background-index=false" ]
            }
            :extensions {
                :autoSetHints true
                :hover_with_actions true
                :inlay_hints {
                    :only_current_line false
                    :only_current_line_autocmd "CursorHold"
                    :show_parameter_hints true
                    :show_variable_name true
                    :parameter_hints_prefix "❰ "
                    :other_hints_prefix "❰# "
                    :max_len_align false
                    :max_len_align_padding 1
                    :right_align false
                    :right_align_padding 7
                    :highlight "Comment"
                }
            }
        }
    }

    {   x.use :mfussenegger/nvim-lint
        :config (lambda []
            (local lint (require :lint))
            (set lint.linters_by_ft {
                :cpp [ :clangtidy :cppcheck :cpplint ]
            })
        )
    }
]
