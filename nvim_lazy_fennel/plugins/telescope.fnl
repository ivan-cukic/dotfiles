(local x (require :core.xvim))

[
    ;; Telescope -- Fuzzy searcher like FZF and CtrlP
    {   x.use :nvim-telescope/telescope.nvim
        :requires [
            :nvim-lua/popup.nvim ;; popup API for nvim
            :nvim-lua/plenary.nvim ;; common LUA functions
            :telescope-fzf-native.nvim ;; Fast Telescope with FZF
            :nvim-telescope/telescope-live-grep-args.nvim ;; RipGrep for Telescope
        ]
        :highlights { :TelescopeBorder        "guifg=#555555"
                      :TelescopePromptTitle   "guifg=#eeeeee guibg=#555555"
                      :TelescopePreviewTitle  "guifg=#888888"
                      :TelescopeResultsTitle  "guifg=#17252c"
                      :TelescopePromptPrefix  "guibg=#17252c"
                      :TelescopePromptCounter "guibg=#17252c"
                      :TelescopePreviewNormal "guibg=#27353c" }
    }
    {   x.use :nvim-telescope/telescope-fzf-native.nvim
        :build :make
        :config (lambda []
            (let [ telescope (require :telescope)
                   telescopeActions (require "telescope.actions") ]
                (telescope.setup {
                    :defaults {
                        :prompt_prefix "   "
                        :sorting_strategy "ascending"
                        :prompt_title ""
                        :preview_title ""
                        :border {}
                        :borderchars {
                            1        [ "─" " " "─" "│" "╭" "─" "╯" "╰" ]
                            :prompt  [ "─" " " " " "│" "╭" "─" " " "│" ]
                            :results [ " " " " "─" "│" "│" " " "─" "╰" ]
                            :preview [ "─" "│" "─" " " "─" "╮" "╯" "─" ]
                        }
                        :selection_caret "> "
                        :layout_config {
                            :prompt_position "top"
                        }
                        :color_devicons false ; konsole clips icons when colour changes
                        :mappings {
                            :i {
                                :<esc>    telescopeActions.close
                                :<c-down> telescopeActions.preview_scrolling_down
                                :<c-up>   telescopeActions.preview_scrolling_up
                            }
                        }
                    }
                    :extensions {
                        :fzf {
                            :fuzzy true
                            :override_generic_sorter true
                            :override_file_sorter true
                            :case_mode :smart_case
                        }
                        :aerial {
                            :show_nesting true
                        }
                    }
                })
                (telescope.load_extension :fzf)
                (telescope.load_extension :aerial)
            )

            ;; TODO: Move to plugins
            (global telescope_switch (fn []
                (let [
                    previewers   (require :telescope.previewers)
                    pickers      (require :telescope.pickers)
                    sorters      (require :telescope.sorters)
                    finders      (require :telescope.finders)

                    picker       (pickers.new {
                        :results_title "Switch"
                        :finder (finders.new_oneshot_job
                            [ (vim.fn.expand "$HOME/bin/_private/nvim-telscope-scripts/most-similar-filenames.py") (vim.api.nvim_buf_get_name 0) ])
                        :sorter (sorters.get_fuzzy_file)
                    })
                ]
                (picker:find))))

            (global telescope_show_modified_on_branch (fn []
                (let [
                    previewers   (require :telescope.previewers)
                    pickers      (require :telescope.pickers)
                    sorters      (require :telescope.sorters)
                    finders      (require :telescope.finders)

                    picker       (pickers.new {
                        :results_title "Modified on current branch"
                        :finder (finders.new_oneshot_job
                            [ (vim.fn.expand "$HOME/bin/_private/nvim-telscope-scripts/git-branch-modified.sh") "list" ])
                        :sorter (sorters.get_fuzzy_file)
                        :previewer (previewers.new_termopen_previewer {
                            :get_command (fn [entry] [(vim.fn.expand "$HOME/bin/_private/nvim-telscope-scripts/git-branch-modified.sh") "diff" entry.value])
                        })
                    })
                ]
                (picker:find))))
        )
    }
    {   x.use :tom-anders/telescope-vim-bookmarks.nvim
        :config (lambda []
                (local telescope (require :telescope))
                (telescope.load_extension :vim_bookmarks))
    }
]
