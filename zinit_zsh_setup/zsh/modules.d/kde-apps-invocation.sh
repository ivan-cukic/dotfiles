
function okular() {
    if [ -z "$1" ];
    then
        env okular &>/dev/null &
    else
        env okular "$1" &>/dev/null &
    fi
}

function dolphin() {
    if [ -z "$1" ];
    then
        env dolphin . &>/dev/null &
    else
        env dolphin "$1" &>/dev/null &
    fi
}

function gwenview() {
    if [ -z "$1" ];
    then
        env gwenview . &>/dev/null &
    else
        env gwenview "$1" &>/dev/null &
    fi
}
