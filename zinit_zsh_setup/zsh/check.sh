function check_command() {
    which $1 > /dev/null || echo "Not found: $1"
}

check_command bat
check_command lsd
check_command fzf
check_command rg
check_command stack
check_command diff-so-fancy
check_command hyperfine
check_command khal
check_command ranger
check_command nnn
check_command task
check_command mycli
check_command pgcli
check_command vifm
check_command fd
check_command bloaty
