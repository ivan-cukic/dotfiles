# Completion styling
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-z}={A-Z} r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' menu select

# History options
HISTSIZE=5000
HISTFILE=~/.zsh_history
SAVEHIST=$HISTSIZE
HISTDUP=erase
setopt appendhistory
setopt sharehistory
setopt hist_ignore_space
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_ignore_dups
setopt hist_find_no_dups

# Other useful options
setopt autocd
setopt correct
setopt noclobber
setopt auto_pushd

# Add user paths after the system's
export PATH=$PATH:~/bin:~/.local/bin

# Default programs
export EDITOR=vim
export PAGER=most

# Locale
export LC_ALL="en_GB.UTF-8"
export LANGUAGE=en_GB

# Go thinks it is normal to create ~/go directory...
export GOPATH=$HOME/.go
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin

# Common shell things
alias rm='rm -i'
alias su='sudo -i'
alias df='dfh-exe'
alias p=popd
alias f='find -iname'
alias fdir='find -type d -iname '

# ls aliases
alias l='lsd'
alias ls='lsd'
alias ll='lsd -l'
alias lr='lsd -t -1 -r'

# Devel
alias make=makeobj
alias gg=rg
alias gig='git grep'

# Trailing aliases
alias -g GG='2>&1 | grep'
alias -g LL="2>&1 | less"
alias -g NUL="&> /dev/null &"
alias -g ...="../.."
alias -g ....="../../.."
alias -g .....="../../../.."

# MPV
alias m=mpv
alias mrecent='ls -t -1 | xargs -d \\n mpv'

# System
alias aptitude='sudo aptitude'
alias reboot='sudo reboot'
alias poweroff='sudo poweroff'

# Git aliases
alias gadd='git add'
alias gbr='git br'
alias gbrall='git brall'
alias gco='echo Use git switch or git restore'
alias gcommit='git commit'
alias glog='git log'
alias gpull='git pull'
alias gst='git status'
alias gdiff='git diff'

# Programs
alias ff="firefox -ProfileManager -no-remote &> /dev/null &"
alias nvim='/usr/bin/nvim -u ~/.nvimrc-6.lua'

# fzf-marks
alias cdb=fzm

# Local modules
for module_file in $HOME/.zsh/modules.d/*; do
    source $module_file
done

for module_file in $HOME/.zsh/post.d/*; do
    zvm_after_init_commands+=("source $module_file;")
done

